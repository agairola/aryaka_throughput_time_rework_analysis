## To run the bottleneck analysis, use the following command on the terminal: 

python -W ignore aryaka_throughput_time_rework_analysis.py -b

## To run the rework analysis, use the following command on the terminal:- 

python -W ignore aryaka_throughput_time_rework_analysis.py -r

## To run both bottleneck and rework analysis, pass both the flags :-

python -W ignore aryaka_throughput_time_rework_analysis.py -b -r


### Suggested improvements : 
    - The bottleneck_gen function should be re-written efficiently, as of now it's the biggest bottleneck in bottleneck analysis.
    - in the __main__ module, code can be refactored to change the location where the final output is dumped based on the STATUS_ONLY flag.
    - break the code into separate bottleneck.py and rework.py for easier comprehension.
