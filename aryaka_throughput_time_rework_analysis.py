import time
from functools import partial
from os.path import exists
import numpy as np
import pandas as pd
from pm4py.algo.discovery.dfg.adapters.pandas import df_statistics
from pm4py.statistics.passed_time.pandas.versions import prepost
from pm4py.util import constants

import config


parameters = config.PARAMETERS


def A_next_B_helper(df, A, B, parameters):
    df = df.copy()
    # caseid_key = (
    #     parameters[constants.PARAMETER_CONSTANT_CASEID_KEY]
    #     if constants.PARAMETER_CONSTANT_CASEID_KEY in parameters
    #     else "case:concept:name"
    # )
    activity_key = (
        parameters[constants.PARAMETER_CONSTANT_ACTIVITY_KEY]
        if constants.PARAMETER_CONSTANT_ACTIVITY_KEY in parameters
        else "concept:name"
    )

    df["@@index"] = df.index
    df_A = df[df[activity_key] == A]
    df_B = df[df[activity_key] == B]
    tot = pd.concat([df_A, df_B]).sort_values("@@index")
    tot["concept:name_2"] = tot[activity_key].shift(-1)
    tot["@@index_diff"] = tot["@@index"].diff().shift(-1)
    tot_2 = tot[
        (tot[activity_key] == A)
        & (tot["concept:name_2"] == B)
        & (tot["@@index_diff"] == 1)
    ]
    if len(tot_2) > 0:
        return df.drop(columns="@@index")


def A_next_B(data: pd.DataFrame, A: str, B: str, parameters: dict) -> pd.DataFrame:
    """
    Returns all the cases where activity A is followed by activity B

    Inputs :-
        data :- log
        A :- Activity A
        B :- Activity B
        parameters :- CAT column names in the log



    """
    caseid_key = (
        parameters[constants.PARAMETER_CONSTANT_CASEID_KEY]
        if constants.PARAMETER_CONSTANT_CASEID_KEY in parameters
        else "case:concept:name"
    )
    activity_key = (
        parameters[constants.PARAMETER_CONSTANT_ACTIVITY_KEY]
        if constants.PARAMETER_CONSTANT_ACTIVITY_KEY in parameters
        else "concept:name"
    )
    data = data.copy()
    df = data[(data[activity_key] == A) | (data[activity_key] == B)]
    tmp_ids = df.groupby(caseid_key).size().reset_index()[caseid_key].unique()
    df = data[data[caseid_key].isin(tmp_ids)].reset_index(drop=True)
    global A_next_B_helper
    A_next_B_helper = partial(A_next_B_helper, A=A, B=B, parameters=parameters)
    x = df.groupby(caseid_key).apply(A_next_B_helper)
    return x


def bottleneck_sample_ids(
    df: pd.DataFrame, bottlenecks_data: pd.DataFrame, parameters: dict
) -> pd.DataFrame:
    """
    Finds two sample ids for each bottleneck pair in bottlenecks data

    df :- log data
    bottlenecks_data :- a dataframe containing the bottleneck activitiy pair
    parameters :- CAT columns for log data
    """
    bottlenecks_data = bottlenecks_data.copy()
    bottlenecks_data["sample_ids"] = None
    for i in range(len(bottlenecks_data)):
        act1 = bottlenecks_data.iloc[i]["activity_1"]
        act2 = bottlenecks_data.iloc[i]["activity_2"]
        case_ids = A_next_B(df, act1, act2, parameters)[
            parameters[constants.PARAMETER_CONSTANT_CASEID_KEY]
        ].unique()
        if len(case_ids) == 0:
            continue
        elif len(case_ids) >= 2:
            ids = ",".join(
                list(
                    map(lambda x: str(x), np.random.choice(case_ids, 2, replace=False))
                )
            )
        else:
            ids = str(np.random.choice(case_ids, 1)[0])

        bottlenecks_data.at[i, "sample_ids"] = ids

    return bottlenecks_data


def add_path_column_to_df(
    df: pd.DataFrame, parameters: dict, column_name="@@path"
) -> pd.DataFrame:
    """
    helper method for bottlenecks gen. Add a path column to dataframe
    df :- log data
    parameters :- CAT columns in the log data
    column_names :- name of the path column


    """

    global activity_key, caseid_key, timestamp_key

    case_id_glue = caseid_key
    attribute_key = activity_key
    filter_df = df[[case_id_glue, attribute_key, timestamp_key]]
    filter_df = filter_df.sort_values(by=[case_id_glue, timestamp_key])
    filter_df_shifted = filter_df.shift(-1)
    filter_df_shifted[case_id_glue] = (
        filter_df_shifted[case_id_glue]
        .fillna(method="ffill")
        .astype(filter_df[case_id_glue].dtype)
    )
    filter_df_shifted.columns = [str(col) + "_2" for col in filter_df_shifted.columns]
    stacked_df = pd.concat([filter_df, filter_df_shifted], axis=1)
    stacked_df = stacked_df[stacked_df[case_id_glue] == stacked_df[case_id_glue + "_2"]]
    stacked_df[column_name] = (
        stacked_df[attribute_key] + "," + stacked_df[attribute_key + "_2"]
    )

    return stacked_df


def bottlenecks_gen(
    df: pd.DataFrame,
    parameters: dict,
    time_metric: str = "hours",
    reverse: bool = True,
    how_many=None,
) -> pd.DataFrame:
    """
    calculates and returns the stats for bottleneck analysis
    inputs :

    df :- event log
    parameters :- log parameters
    time_metric :- unit of time in which throughput time is to be returned
    reverse :- sort order, descending by default
    how_many :- returns top n results, returns all by default


    """
    case_id_glue = (
        parameters[constants.PARAMETER_CONSTANT_CASEID_KEY]
        if constants.PARAMETER_CONSTANT_CASEID_KEY in parameters
        else "case:concept:name"
    )

    bottlenecks = get_bottlenecks(
        df,
        time_metric=time_metric,
        reverse=reverse,
        how_many=how_many,
        parameters=parameters,
    )
    bottlenecks_list = []
    tot_case_num = len(np.unique(df[case_id_glue]))
    #     parameters[PARAMETER_CONSTANT_ATTRIBUTE_KEY] = parameters[
    #         PARAMETER_CONSTANT_ACTIVITY_KEY]
    stacked_df = add_path_column_to_df(df, parameters)
    for bottleneck in bottlenecks:
        act1, act2 = bottleneck[0]
        path = [act1 + "," + act2]
        stacked_df1 = stacked_df[stacked_df["@@path"].isin(path)]
        num_cases_aff = len(np.unique(stacked_df1[case_id_glue]))
        bottleneck_dict = {
            "activity_1": act1,
            "activity_2": act2,
            "avg_throughput_time": round(bottleneck[1], 2),
            "total_cases_affected": num_cases_aff,
            "total_impact": num_cases_aff * round(bottleneck[1], 2),
            "total_cases": tot_case_num,
            "perc_cases_affected": round(((num_cases_aff * 100) / tot_case_num), 2),
        }
        bottlenecks_list.append(bottleneck_dict)

    return bottlenecks_list


def get_bottlenecks(
    df: pd.DataFrame,
    time_metric: str = "hours",
    reverse: bool = True,
    how_many: int = None,
    parameters: dict = None,
):
    """
    calls the dfg graph from pm4py to calculate the performance(throughput time) Directly Follows Graph

    df : input log
    time_metric : unit of time in which to return the throughput time
    reverse : parameter to sort throughput time in an ascending / descending order. Defaults to descending
    parameters :- log parameters
    how_many :- how many bottleneck results to return. None returns all the results.


    """
    if time_metric not in config.TIME_METRICS.keys():
        raise ValueError("time metric must be in days, minutes or hours")
    pairwise_throughput_time = dict(
        df_statistics.get_dfg_graph(
            df,
            measure="performance",
            activity_key=parameters[constants.PARAMETER_CONSTANT_ACTIVITY_KEY],
            case_id_glue=parameters[constants.PARAMETER_CONSTANT_CASEID_KEY],
            timestamp_key=parameters[constants.PARAMETER_CONSTANT_TIMESTAMP_KEY],
        )
    )
    bottlenecks = {
        k: v / config.TIME_METRICS[time_metric]
        for k, v in pairwise_throughput_time.items()
    }
    bottlenecks = sorted(bottlenecks.items(), key=lambda kv: kv[1], reverse=reverse)
    if how_many is None:
        return bottlenecks
    else:
        return bottlenecks[:how_many]


def get_bottleneck_data(data, parameters):
    """
    Wrapper function to get bottleneck data

    """
    bottlenecks_data = (
        pd.DataFrame(
            bottlenecks_gen(
                data,
                time_metric="hours",
                reverse=True,
                how_many=None,
                parameters=parameters,
            )
        )
        .sort_values("total_impact", ascending=False)
        .reset_index(drop=True)
    )
    return bottlenecks_data


def get_case_activity_counts(df: pd.DataFrame, parameters: dict) -> pd.DataFrame:
    """
    Calculates the number of activity (A,B) pairs in a given case, along with throughput time

    df :- A single case from the log
    params :- log parameters
    """
    activity_key = (
        parameters[constants.PARAMETER_CONSTANT_ACTIVITY_KEY]
        if constants.PARAMETER_CONSTANT_ACTIVITY_KEY in parameters
        else "concept:name"
    )
    timestamp_key = (
        parameters[constants.PARAMETER_CONSTANT_TIMESTAMP_KEY]
        if constants.PARAMETER_CONSTANT_TIMESTAMP_KEY in parameters
        else "time:timestamp"
    )

    df = df.copy()
    df["concept:name_2"] = df[activity_key].shift(-1)
    df["throughput_time"] = df[timestamp_key].diff().shift(-1)
    tmp = df[activity_key].value_counts().reset_index().to_dict("records")
    counts = {k["index"]: k[activity_key] for k in tmp}
    df["counts"] = df[activity_key].map(counts.get)

    return df


def get_rework_stats(df: pd.DataFrame, parameters: dict) -> pd.DataFrame:
    """
    Caclulates all the rework stats in the process log

    """
    df["throughput_time"] = df["throughput_time"] / pd.Timedelta(1, "h")
    avg_throughput_time = (
        df.groupby(parameters[constants.PARAMETER_CONSTANT_ACTIVITY_KEY])
        .agg({"throughput_time": "mean"})
        .round(2)
        .reset_index()
    )
    # avg_throughput_time['throughput_time'] = avg_throughput_time['throughput_time'].apply(lambda x: pd.Timedelta(seconds=x)) / pd.Timedelta(1, 'h')
    total_count = (
        df.groupby(parameters[constants.PARAMETER_CONSTANT_ACTIVITY_KEY])
        .size()
        .sort_values(ascending=False)
        .reset_index(name="counts")
    )
    avg_thpt_time = (
        avg_throughput_time.sort_values("throughput_time", ascending=False),
    )

    total_counts = total_count.sort_values("counts", ascending=False)

    new_valid_results = pd.merge(
        avg_thpt_time[0],
        total_counts,
        on=parameters[constants.PARAMETER_CONSTANT_ACTIVITY_KEY],
        how="inner",
    )

    new_valid_results["total_impact"] = (
        new_valid_results["throughput_time"] * new_valid_results["counts"]
    ).astype(int)

    new_valid_results = new_valid_results.sort_values("total_impact", ascending=False)
    new_valid_results["avg_number_of_occurance"] = (
        new_valid_results["counts"]
        / data[parameters[constants.PARAMETER_CONSTANT_CASEID_KEY]].nunique()
    )

    return new_valid_results


def rework_sample_ids(df: pd.DataFrame, valid_results: pd.DataFrame) -> pd.DataFrame:
    """
    finds two caseids from the log for each rework identified in valid results

    REMARK :- I have hardcoded the function to return 2 sample caseids only. You can parameterize the number of
              caseids to return by passing a how_many function in the function definition.

    """
    global activity_key, caseid_key
    sample_ids = {}
    rework_activities = valid_results[activity_key].unique()
    for act in rework_activities:
        act_ids = df[df[activity_key] == act][caseid_key].unique()

        if len(act_ids) >= 2:
            sample_ids[act] = ",".join(
                list(map(lambda x: str(x), np.random.choice(act_ids, 2, replace=False)))
            )
        else:
            sample_ids[act] = str(np.random.choice(act_ids, 1)[0])

    valid_results["sample_ids"] = valid_results[activity_key].map(sample_ids.get)
    return valid_results


def add_local_and_global_rework_counts(rework, bottleneck, parameters):
    rework = rework.copy()
    bottleneck = bottleneck.copy()

    bottleneck_rework = bottleneck[
        bottleneck["activity_1"] == bottleneck["activity_2"]
    ][["activity_1", "avg_throughput_time", "total_cases_affected"]]
    bottleneck_rework.rename(
        columns={
            "activity_1": parameters[constants.PARAMETER_CONSTANT_ACTIVITY_KEY],
            "avg_throughput_time": "local_avg_throughput_time",
            "total_cases_affected": "local_total_cases_affected",
        },
        inplace=True,
    )

    merged_rework = pd.merge(
        rework,
        bottleneck_rework,
        how="left",
        on=parameters[constants.PARAMETER_CONSTANT_ACTIVITY_KEY],
    ).fillna(0)
    return merged_rework


if __name__ == "__main__":
    caseid_key = parameters[constants.PARAMETER_CONSTANT_CASEID_KEY]
    timestamp_key = parameters[constants.PARAMETER_CONSTANT_TIMESTAMP_KEY]
    activity_key = parameters[constants.PARAMETER_CONSTANT_ACTIVITY_KEY]

    # Reading the file
    data = pd.read_csv(config.ARYAKA_FILE)
    data[timestamp_key] = pd.to_datetime(data[timestamp_key])
    data = data[
        [caseid_key, timestamp_key, activity_key]
    ]  # selects CAT columns as defined in the config file
    data = data.sort_values(
        [caseid_key, timestamp_key]
    )  # sorts the log by caseid and timestamp
    print(f"file shape and unique activities: {data.shape, data[caseid_key].nunique()}")

    # check functionality
    if config.STATUS_ONLY:
        print(f"filtering all the non Status and non Created. events from the log ..")
        orig_data = pd.read_csv(config.ORIG_ARYAKA_FILE)
        status_act = list(
            set(orig_data["Old Value"].unique()).union(
                set(orig_data["New Value"].unique())
            )
        )[1:]
        status_act.append("Created")
        status_act.append("Impact Assessment")
        data = data[~data[activity_key].isin(status_act)]
        data.to_csv("dump1.csv", index=False)
        print(f"file shape after removing all the non Status activities : {data.shape}")

    # filter out cases that don't begin with config.START_ACTIVITY and ends with config.END_ACTIVITY
    ids_ = data.groupby(caseid_key)[activity_key].agg(["first", "last"])
    ids_prop = ids_[
        (ids_["first"] == config.START_ACTIVITY) & (ids_["last"] == config.END_ACTIVITY)
    ].reset_index()
    print(
        f"file shape and unique cases after excluding incomplete cases : {data.shape, data[caseid_key].nunique()}"
    )
    data = data[data[caseid_key].isin(ids_prop[caseid_key])].reset_index(drop=True)

    # drop out all activities from the log which are contained in config.ACTIVITIES_TO_REMOVE
    data = data[~data[activity_key].isin(config.ACTIVITIES_TO_REMOVE)].reset_index(
        drop=True
    )
    print(
        f"file shape after excluding activities defined in ACTIVITIES_TO_REMOVE : {data.shape, data[caseid_key].nunique()}"
    )

    # bottleneck calculations
    if config.ARGS.bottleneck:
        print(f"shape: {data.shape}")
        print("bottleneck calculation...")
        t = time.time()
        bottlenecks_data = get_bottleneck_data(data, parameters)

        # add sample id column to bottlenecks_data
        bottleneck_data_with_sample_ids = bottleneck_sample_ids(
            data, bottlenecks_data, parameters
        )
        bottleneck_data_with_sample_ids.to_csv(
            config.BOTTLENECK_OUTPUT_FILEPATH, index=False
        )
        print(f"bottleneck output written to {config.BOTTLENECK_OUTPUT_FILEPATH}")
        print(f"calculation finished in {time.time() - t} seconds")

    # Rework Calculation
    if config.ARGS.rework:
        print("rework calculation..")
        t = time.time()
        get_case_activity_counts = partial(
            get_case_activity_counts, parameters=parameters
        )

        # get activity counts for each pair of transitions in the log
        rework_counts = (
            data.groupby(parameters[constants.PARAMETER_CONSTANT_CASEID_KEY])
            .apply(get_case_activity_counts)
            .reset_index(drop=True)
        )
        # keep only those activities that happen more than once per log
        rework_counts_valid = rework_counts[rework_counts["counts"] > 1]
        activity_key = parameters[constants.PARAMETER_CONSTANT_ACTIVITY_KEY]
        max_counts_for_each_activity = (
            rework_counts_valid[[activity_key, "counts"]]
            .groupby(parameters[constants.PARAMETER_CONSTANT_ACTIVITY_KEY])
            .max()
            .reset_index()
        )
        # count the max occurance for each activity in the log
        max_counts_for_each_activity.rename(
            columns={"counts": "max_rework"}, inplace=True
        )
        new_valid_results = get_rework_stats(rework_counts_valid, parameters)

        # merge the results
        new_valid_results = pd.merge(
            new_valid_results,
            max_counts_for_each_activity,
            on=parameters[constants.PARAMETER_CONSTANT_ACTIVITY_KEY],
            how="inner",
        )

        # get sample ids
        rework_with_sample_ids = rework_sample_ids(
            rework_counts_valid, new_valid_results
        )
        if "bottleneck_data_with_sample_ids" not in globals():
            if exists(config.BOTTLENECK_OUTPUT_FILEPATH):
                print(f"reading {config.BOTTLENECK_OUTPUT_FILEPATH}")
                bottleneck_data_with_sample_ids = pd.read_csv(
                    config.BOTTLENECK_OUTPUT_FILEPATH
                )
            else:
                raise FileNotFoundError(
                    f"{config.BOTTLENECK_OUTPUT_FILEPATH} not found"
                )

        # find local rework counts by looking at bottleneck data. Any bottleneck where
        # the same activities happen consecutively is defined as local rework
        rework_with_local_global_counts = add_local_and_global_rework_counts(
            rework_with_sample_ids,
            bottleneck_data_with_sample_ids,
            parameters,
        )
        rework_with_local_global_counts.to_csv(
            config.REWORK_OUTPUT_FILEPATH, index=False
        )
        print(f"rework output written to {config.REWORK_OUTPUT_FILEPATH}")
        print(f"total time taken : {time.time() - t} seconds")
