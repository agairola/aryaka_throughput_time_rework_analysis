from pm4py.util import constants
import argparse

# flag to include only 'Status' and 'Created.' events from the original dataset
STATUS_ONLY = False

# Location of the preprocessed aryaka file
ARYAKA_FILE = "/home/abhijit/Desktop/optimal_process_generation/index_aryaka_status.csv"
# Location of the original aryaka file before preprocessing
ORIG_ARYAKA_FILE = "/home/abhijit/Downloads/change_cases_status.csv"

# output location for the bottleneck analysis
BOTTLENECK_OUTPUT_FILEPATH = "bottleneck_analysis.csv"
BOTTLENECK_OUTPUT_FILEPATH_STATUS = "bottleneck_analysis_status_only_final.csv"

# output location for rework analysis
REWORK_OUTPUT_FILEPATH = "rework_analysis.csv"
REWORK_OUTPUT_FILEPATH_STATUS = "rework_analysis_status_only_final.csv"
DUMP = "dump.csv"


START_ACTIVITY = "Created"
END_ACTIVITY = "Closed"

ACTIVITIES_TO_REMOVE = [
    "Custom Case Flag changed",
    "Record Locked",
    "Record Unlocked",
    "Case Summary Updated",
    "Case Origin Changed",
    "Description updated",
    "Send Initial Email changed",
    "Shipment Address changed",
    "Starttime changed",
    "Endtime changed",
]

PARAMETERS = {
    constants.PARAMETER_CONSTANT_ACTIVITY_KEY: "activity",
    constants.PARAMETER_CONSTANT_TIMESTAMP_KEY: "Edit Date",
    constants.PARAMETER_CONSTANT_CASEID_KEY: "Case Number",
}

TIME_METRICS = {"hours": 60 * 60, "seconds": 1, "days": 60 * 60 * 24, "minutes": 60}


parser = argparse.ArgumentParser(description="Calculates Bottleneck and Rework stats")

parser.add_argument(
    "-b",
    "--bottleneck",
    action="store_true",
    help="flag for bottleneck calculation",
)
parser.add_argument(
    "-r",
    "--rework",
    action="store_true",
    help="flag for calculating rework",
)

ARGS = parser.parse_args()